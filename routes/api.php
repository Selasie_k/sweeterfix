<?php

if (App::environment('production')) {
    URL::forceScheme('https');
}

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user/orders/{user}', 'UserAccountController@getOrders')->middleware('auth')->name('user.orders');
Route::post('/user/address/{user}', 'UserAccountController@addAddress')->name('user.address.add');
Route::get('/user/address/{user}', 'UserAccountController@getAddresses')->name('user.addresses');
Route::delete('/user/address/{address}', 'UserAccountController@deleteAddress')->name('user.address.delete');
Route::patch('/user/address/{address}', 'UserAccountController@updateAddress')->name('user.address.update');

Route::post('/order','OrderController@create');
Route::post('/search_location','CustomerController@search_location');

Route::prefix('/shop')->group(function () {
    Route::namespace('Shop')->group(function() {
        Route::get('/orders', 'ShopController@apiOrders');
    });
});
