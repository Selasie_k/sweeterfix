<?php

if (App::environment('production')) {
    URL::forceScheme('https');
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'ShoppingController@showLandingPage');

Route::middleware('auth:web')->get('/checkout', 'ShoppingController@showCheckoutPage')->name('checkout');
Route::post('/checkout', 'ShoppingController@checkout')->name('checkout.submit');

Auth::routes();

Route::namespace('Auth')->group(function() {
    Route::get('/admin/login', 'AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/admin/login', 'AdminLoginController@login')->name('admin.login.submit');  

    Route::get('/bakery/login', 'ShopLoginController@showLoginForm')->name('shop.login');
    Route::post('/bakery/login', 'ShopLoginController@login')->name('shop.login.submit');

    Route::get('/courier/login', 'CourierLoginController@showLoginForm')->name('courier.login');
    Route::post('/courier/login', 'CourierLoginController@login')->name('courier.login.submit');
});

Route::resource('/shop', 'ShoppingController');

Route::get('/user/{user}/account', 'UserAccountController@show')->name('user.profile');
Route::get('/order/{order}/track', 'OrderController@track')->middleware('auth')->name('order.track');


/*Route for bakeires/shops */
Route::prefix('bakery')->group(function() {
    Route::namespace('Shop')->group(function() {
        Route::get('/', 'ShopController@dashboard')->name('shop.dashboard');        
        Route::get('/orders', 'ShopController@orders')->name('shop.orders');        
    });
});


/** admin routes */
Route::prefix('admin')->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('/', 'HomeController@index')->name('admin.dashboard');
        Route::get('/data', function() { return 'all system data'; })->middleware('auth:admin'); // TEMPORARY
    });
});



