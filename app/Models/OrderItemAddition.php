<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemAddition extends Model
{
    protected $guarded = [];

    public function order_item()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
