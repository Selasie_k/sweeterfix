<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemOption extends Model
{
    protected $guarded = [];

    public function order_item()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
