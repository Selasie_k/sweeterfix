<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function option()
    {
        return $this->hasOne(OrderItemOption::class);
    }

    public function additions()
    {
        return $this->hasMany(OrderItemAddition::class);
    }
}
