<?php

namespace App\Models;

use App\Shop;
use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{
    protected $guarded = [];

    public function shops(){
        return $this->hasMany(Shop::class);
    }
}
