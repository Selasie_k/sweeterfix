<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(ProductCategory::class);
    }

    public function additions(){
        return $this->hasMany(ProductAddition::class);
    }

    public function options(){
        return $this->hasMany(ProductOption::class);
    }
}
