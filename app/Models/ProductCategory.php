<?php

namespace App\Models;

use App\Shop;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $guarded = [];

    public function product(){
        return $this->hasMany(Product::class);
    }

    public function shop(){
        return $this->belongsTo(Shop::class);
    }
}
