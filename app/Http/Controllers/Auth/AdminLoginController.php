<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{

    public function __construct(){

        $this->middleware('guest:admin');
        
    }

    public function showLoginForm(){
        return view('admin.auth.login');
    }

    public function login(Request $request){

        // validate credentials
        $this->validate($request, [
            'email' => 'required|email|string',
            'password' => 'required|string'
        ]);

        $credentials = $request->only('email', 'password');

        // attempt to login
        if (Auth::guard('admin')->attempt($credentials, $request->remember)) {

            // send the user to their intended page if not homepage
            return redirect()->intended(route('admin.dashboard'));
        } 

        return back()->withInput($request->only('email', 'password'));
    }

    public function logout(Request $request){

    }
}
