<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShopLoginController extends Controller
{

    public function __construct(){

        $this->middleware('guest:shop');
        
    }

    public function showLoginForm(){
        return view('shop.auth.login');
    }

    public function login(Request $request){

        // validate credentials
        $this->validate($request, [
            'email' => 'required|email|string',
            'password' => 'required|string'
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // attempt login
        if (Auth::guard('shop')->attempt($credentials, $request->remember)) {
            // send the user to their intended page if not then homepage
            return redirect()->intended(route('shop.dashboard'));
        } 

        return back()->withInput($request->only('email', 'password'));
    }

    public function logout(Request $request){

    }
}
