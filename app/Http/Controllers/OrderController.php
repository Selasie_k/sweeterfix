<?php

namespace App\Http\Controllers;

use Stripe\Stripe;
use App\Models\Order;
use App\Events\NewOrder;
use Illuminate\Support\Str;
use App\Mail\OrderConfirmed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // store a new order
        
        // return response()->json($request->all(), 200);
        // return auth('api')->user()->id;
        $order = Order::create([
            'number' => strtoupper(Str::random(7)), 
            'user_id' => auth('api')->user()->id,
            'sub_total' => $request->subTotal,
            'service_fee' => $request->serviceFee,
            'shop_id' => $request->shop_id,
            'total' => $request->charge / 100, // convert cents back to dollars 
            'shop_note' => $request->specialInstructions,
        ]);

        foreach ($request->items as $item) {
            $order_item = $order->items()->create([
                'name' => $item['name'],
                'price' => $item['price'],
                'subtotal' => $item['subTotal'] * $item['quantity'],
                'quantity' => $item['quantity'],
            ]);
            
            if($item['option']){
                $order_item->option()->create([
                    'name' => $item['option']['name'],
                    'price' => $item['option']['price'],
                ]);
            }

            if ($order_item['additions']) {
                foreach($item['additions'] as $addition){
                    $order_item->additions()->create([
                        'name' => $addition['name'],
                        'price' => $addition['price'],
                    ]);
                }
            }
        }

        // return response()->json($order, 200);
        
        // Mail::to( 'user@sweeterfix.com' )->send(new OrderConfirmed($order));
        event(new NewOrder($order));
        return response()->json(auth('api')->user()->id);
        
        dd($request->all());
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        try {
            $charge = \Stripe\Charge::create([
                'amount' => $request->charge,
                'currency' => 'usd',
                'description' => 'Order from SweeterFix. Order ID: ' . $order->number, 
                'source' => $request->stripeToken,
            ]);
            return response()->json($charge, 200);
        } catch (Exception $e) {
            //
        }
    }

    /**
     * Track the specified order.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function track(Order $order)
    {
        return \response()->json($order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        // show an order for editing
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        // update an order status
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        // delete an order
    }
}
