<?php

namespace App\Http\Controllers;

use App\Models\Shop;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{

    private $bakeries = [
            [
                'id' => 1,
                'logo' => 'crema.png',
                'name' => 'Crema Pastry',
                'address' => '421 Greenbrook Dr, Kitchener, On, Canada',
                'status' => 'open',
                'category' => 'Pastries',
            ],
            [
                'id' => 2,
                'logo' => 'owl.jpg',
                'name' => 'The Healthy Owl',
                'address' => '620 Davenport Rd, Waterloo, On, Canada',
                'status' => 'closed',
                'category' => 'Treats',
            ],
            [
                'id' => 3,
                'logo' => 'ce_food.jpg',
                'name' => 'Ce Food Experience',
                'address' => '136 Moore Ave S, Waterloo, On, Canada',
                'status' => 'open',
                'category' => 'Treats',
            ],
            [
                'id' => 4,
                'logo' => 'shop_rite.jpg',
                'name' => 'Bake Shop',
                'address' => '136 Moore Ave S, Waterloo, On, Canada',
                'status' => 'closed',
                'category' => 'Healthy Snacks',
            ],
        ];

    /**
     * Show the application landing page.
     *
     * @return view
     */
    public function index()
    {
        
    }

    /**
     * Show list of bakeries in specified city.
     *
     * @return view
     */
    public function bakeries($address = null)
    {
  
    }

    // public function getAllBakeries()
    // {
    //     return $this.bakeries 
    // }

    public function show_bakery($name = null)
    {
        $categories = [
            'Cake Slices',
            'Cake Slab',
            'Pastries',
            'Cookies',
            'Wafers',
            'Assorted Tray',
            'Savarina',
            'Amandina',
            'Isler',
            'Bukuresti',
            'Eclairs',
            'Cream Horns',
        ];

        $pastries = [
            [
                'id' => 1,
                'name' => 'Vanilla Cake',
                'description' => 'Lorem ipsum dolor sit amet.',
                'photo' => 'index.png',
                'price' => 0,
                'options' => [
                    'sizes' => [
                        [
                            'name' => '5 inch',
                            'value' => 11.5
                        ],
                        [
                            'name' => '6 inch',
                            'value' => 22.5
                        ],
                        [
                            'name' => '7 inch',
                            'value' => 36.2
                        ],
                        [
                            'name' => '8 inch',
                            'value' => 38.6
                        ],
                    ],
                    
                    'shapes' => [
                        [ 
                            'name' => 'circular',
                            'value' => 0
                        ],
                        [ 
                            'name' => 'square',
                            'value' => 0
                        ],
                    ],
                    'additions' => [
                        [
                            'name' => 'Extra Chocolate',
                            'value' => 2
                        ],
                        [
                            'name' => 'Thick icing',
                            'value' => 2.5
                        ],
                        [
                            'name' => 'Sparkles',
                            'value' => 0.5
                        ],
                    ],
                ],
            ],
            [
                'id' => 2,
                'name' => 'Cherry Tart Cupcakes',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, ratione?',
                'photo' => 'index1.png',
                'price' => 6.50,
                'options' => [
                    'sizes' => [
                        [
                            'name' => 'Dozen',
                            'value' => 50
                        ],
                        [
                            'name' => 'Half Dozen',
                            'value' => 25
                        ],
                    ],
                    'additions' => [
                        [
                            'name' => 'Extra Chocolate',
                            'value' => 2
                        ],
                        [
                            'name' => 'extra Sparkles',
                            'value' => 0.5
                        ],
                    ],
                ],
            ],
            [
                'id' => 3,
                'name' => 'Chocolate Chip (12 pcs)',
                'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
                'photo' => 'index2.png',
                'price' => 9.00,
                'options' => [
                    'sizes' => [
                        [
                            'name' => '5"',
                            'value' => 1.5
                        ],
                        [
                            'name' => '6"',
                            'value' => 2.5
                        ],
                        [
                            'name' => '7"',
                            'value' => 3.2
                        ],
                        [
                            'name' => '8"',
                            'value' => 3.6
                        ],
                    ],
                    
                    'shapes' => [
                        [ 
                            'name' => 'circular',
                            'value' => 0
                        ],
                        [ 
                            'name' => 'square',
                            'value' => 0
                        ],
                    ],
                    'flavors' => [
                        [
                            'name' => 'vanilla',
                            'value' => 1.0
                        ],
                        [
                            'name' => 'strawberry',
                            'value' => 1.0
                        ],
                    ],
                    'additions' => [
                        [
                            'name' => 'Extra Chocolate',
                            'value' => 2
                        ],
                        [
                            'name' => 'Thick icing',
                            'value' => 2.5
                        ],
                        [
                            'name' => 'Sparkles',
                            'value' => 0.5
                        ],
                    ],
                ],
            ],
            [
                'id' => 4,
                'name' => 'Assorted Monster Cookies',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam in quibusdam deserunt.',
                'photo' => 'shop3.png',
                'price' => 10.00,
                'options' => [
                    'sizes' => [
                        [
                            'name' => '5"',
                            'value' => 1.5
                        ],
                        [
                            'name' => '6"',
                            'value' => 2.5
                        ],
                        [
                            'name' => '7"',
                            'value' => 3.2
                        ],
                        [
                            'name' => '8"',
                            'value' => 3.6
                        ],
                    ],
                    
                    'shapes' => [
                        [ 
                            'name' => 'circular',
                            'value' => 0
                        ],
                        [ 
                            'name' => 'square',
                            'value' => 0
                        ],
                    ],
                ],
            ],
            [
                'id' => 5,
                'name' => 'Assorted Monster Cookies',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam in quibusdam deserunt.',
                'photo' => 'index5.png',
                'price' => 10.00,
                'options' => [
                    'sizes' => [
                        [
                            'name' => '5"',
                            'value' => 1.5
                        ],
                        [
                            'name' => '6"',
                            'value' => 2.5
                        ],
                        [
                            'name' => '7"',
                            'value' => 3.2
                        ],
                        [
                            'name' => '8"',
                            'value' => 3.6
                        ],
                    ],
                    
                    'shapes' => [
                        [ 
                            'name' => 'circular',
                            'value' => 0
                        ],
                        [ 
                            'name' => 'square',
                            'value' => 0
                        ],
                    ],
                ],
            ],
            [
                'id' => 6,
                'name' => 'Assorted Monster Cookies',
                'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laboriosam in quibusdam deserunt.',
                'photo' => 'index6.png',
                'price' => 10.00,
                'options' => [
                    'sizes' => [
                        [
                            'name' => '5"',
                            'value' => 1.5
                        ],
                        [
                            'name' => '6"',
                            'value' => 2.5
                        ],
                        [
                            'name' => '7"',
                            'value' => 3.2
                        ],
                        [
                            'name' => '8"',
                            'value' => 3.6
                        ],
                    ],
                    
                    'shapes' => [
                        [ 
                            'name' => 'circular',
                            'value' => 0
                        ],
                        [ 
                            'name' => 'square',
                            'value' => 0
                        ],
                    ],
                    'additions' => [
                        [
                            'name' => 'Extra Chocolate',
                            'value' => 2
                        ],
                        [
                            'name' => 'Thick icing',
                            'value' => 2.5
                        ],
                        [
                            'name' => 'Sparkles',
                            'value' => 0.5
                        ],
                    ],
                ],
            ],
        ];

        $key = array_search($name, array_column($this->bakeries, 'name'));
        $bakery = $this->bakeries[$key];
        // dd(json_encode($pastries));
        // return $pastries;
        return view('customer.bakery', compact('bakery', 'categories', 'pastries'));
    }

    public function checkout(Request $request) {
        // dd($request->all());
        session(['cart' => $request->all()]);
        // return 'saved in session';
    }

    public function show_checkout() { 
        
    }

    public function search_location(Request $request){
        return \response($request->all());
    }
}
