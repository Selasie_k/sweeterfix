<?php

namespace App\Http\Controllers;

use App\User;
use App\UserAddress;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAccountController extends Controller
{

    public function __construct(){
        // $this->middleware('auth');
    }

    public function show(User $user)
    {
        if($user->id == Auth::guard('web')->id()){
            return view('user.profile')
                ->with([
                    'contrast_nav' => true, 
                    'user' => $user->with(['orders.items', 'orders.shop'])
                ->find($user->id)]);
        }
        return redirect()->back()->withErrors(['unauthorized' => 'You are not the owner of that account']);
    }

    public function getOrders(User $user) {
        // return \response()->json(Order::all());
        return \response()->json($user->orders);
    }

    public function addAddress(Request $request, User $user){
        $request->validate([
            //
        ]);

        return $user->addresses()->create($request->all());
    }

    public function getAddresses(User $user) {
        return $user->addresses;
    }

    public function deleteAddress(UserAddress $address){
        $address->delete();
        return 'delete operation successful';
    }

    public function updateAddress(Request $request, UserAddress $address){

        // validate input

        // update record

        $address->update($request->all());

        return 'true';
    }

}
