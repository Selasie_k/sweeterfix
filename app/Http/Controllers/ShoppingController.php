<?php

namespace App\Http\Controllers;

use Auth;
use App\Shop;
use Illuminate\Http\Request;

class ShoppingController extends Controller
{
    public function __construct(){
        // $this->middleware('auth')->only(['showCheckoutPage']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $bakeries = $this->bakeries;
        // $bakeries = Shop::with(['products.additions','products.options'])->get();
        $shops = Shop::with(['category'])->get();
        return view('customers.shops', compact('shops'));
    }

    public function showLandingPage(){
        // tell the search component it is on the landing page so it doesnt send and ajax request
        $isHomePage = 1;
        return view('welcome')->with('isHomePage', $isHomePage);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        $shop = $shop->with('products.additions','products.options')->find($shop->id);
        return view('customers.shop')->with([
            'shop' => $shop,
            ]);
    }

    public function showCheckoutPage(){

        $contrast_nav = true;

        $deliveryFees = [
            [
                'min' => 0,
                'max' => 5,
                'fee' => 6
            ],
            [
                'min' => 5,
                'max' => 8,
                'fee' => 6.5
            ],
            [
                'min' => 8,
                'max' => 10,
                'fee' => 7
            ],
            [
                'min' => 10,
                'max' => 12,
                'fee' => 7.5
            ],
            [
                'min' => 12,
                'max' => 14,
                'fee' => 8
            ],
            [
                'min' => 14,
                'max' => 16,
                'fee' => 8.5
            ],
            [
                'min' => 16,
                'max' => 20,
                'fee' => 9
            ],
        ];

        $timeWindow = [
            [
                'from' => '1:00pm',
                'to' => '2:00pm'
            ],
            [
                'from' => '2:00pm',
                'to' => '4:00pm'
            ],
            [
                'from' => '4:00pm',
                'to' => '6:00pm'
            ],
        ];

        $user = Auth::user();

        // return $user;
        return view('customers.checkout')
                ->with([
                    'contrast_nav' => $contrast_nav, 
                    'deliveryFees' => $deliveryFees,
                    'timeWindow' => $timeWindow,
                    'user' => $user
                ]);
    }

    public function checkout(){

    }
}
