<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // redirect user to appropriate homepage if they try to access the 
        // login page while authenticated with particular guard
        // dd(\Session::get('url.intended'));
        // dd($guard);
        switch ($guard) {
            case 'admin':
            if (Auth::guard($guard)->check()) {
                return redirect()->route('admin.dashboard');
            }
            break;
            case 'shop':
            if (Auth::guard($guard)->check()) {
                return redirect()->route('shop.dashboard');
            }
            break;
        
            default:
            
            if (Auth::guard($guard)->check()) {
                // return redirect('/home');
                return redirect('/');
            }
            break;
        }

        // if (Auth::guard($guard)->check()) {
        //     return redirect('/home');
        // }

        return $next($request);
    }
}
