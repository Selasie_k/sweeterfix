<?php

namespace App;

use App\Models\Order;
use App\Models\Product;
use App\Models\ShopCategory;
use App\Models\ProductCategory;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Shop extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'phone', 'password', 'logo', 'banner', 'shop_category', 'address', 'city'
    // ];

    // TEMPORARY
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function product_categories(){
        return $this->hasMany(ProductCategory::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function category(){
        return $this->belongsTo(ShopCategory::class);
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }
}
