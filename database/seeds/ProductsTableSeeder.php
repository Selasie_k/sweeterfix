<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'category_id' => 1,
                'shop_id' => 1,
                'name' => 'Vanilla Cake',
                'description' => 'Lorem ipsum dolor sit amet.',
                'photo' => 'index.png',
                'price' => 0,
                'max_quantity' => 50,
                'order_window' => 3,
                'visible' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'category_id' => 2,
                'shop_id' => 1,
                'name' => 'Cherry Tart Cupcakes',
                'description' => 'Lorem ipsum dolor sit amet.',
                'photo' => 'index1.png',
                'price' => 0,
                'max_quantity' => 50,
                'order_window' => 3,
                'visible' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],

            
            [
                'category_id' => 3,
                'shop_id' => 1,
                'name' => 'Chocolate Chip',
                'description' => 'Lorem ipsum dolor sit amet.',
                'photo' => 'index2.png',
                'price' => 0,
                'max_quantity' => 50,
                'order_window' => 3,
                'visible' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'category_id' => 1,
                'shop_id' => 1,
                'name' => 'Assorted Monster Cookies',
                'description' => 'Lorem ipsum dolor sit amet.',
                'photo' => 'shop3.png',
                'price' => 10,
                'max_quantity' => 50,
                'order_window' => 3,
                'visible' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'category_id' => 1,
                'shop_id' => 1,
                'name' => 'Macaron',
                'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
                'photo' => 'index2.png',
                'price' => 0,
                'max_quantity' => 50,
                'order_window' => 3,
                'visible' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],

            // [
            //     'category_id' => 2,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],

            // [
            //     'category_id' => 2,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => '',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'category_id' => 2,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => '',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],

            // [
            //     'category_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => '',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'category_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => '',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],

            // [
            //     'category_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => '',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'category_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '',
            //     'description' => '',
            //     'photo' => '',
            //     'price' => 0,
            //     'max_quantity' => 50,
            //     'order_window' => 3,
            //     'visible' => true,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],


        ]);
    }
}
