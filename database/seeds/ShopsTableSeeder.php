<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->insert([
            [
                'name' => 'Crema Pastry',
                'phone' => '000 000 000',
                'email' => 'crema@pastry.com',
                'logo' => 'crema.png',
                'banner' => '',
                'address' => '421 Greenbrook Dr, Kitchener, On, Canada',
                'city' => 'Ontario',
                'category_id' => 1,
                'open_status' => true,
                'password' => bcrypt('12345678'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'The Healthy Owl',
                'phone' => '111 1111 1111',
                'email' => 'healthyowl@treats.com',
                'logo' => 'owl.jpg',
                'banner' => '',
                'address' => '620 Davenport Rd, Waterloo, On, Canada',
                'city' => 'Ontario',
                'category_id' => 1,
                'open_status' => true,
                'password' => bcrypt('12345678'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Ce Food Experience',
                'phone' => '222 2222 2222',
                'email' => 'ce@food.com',
                'logo' => 'ce_food.jpg',
                'banner' => '',
                'address' => '136 Moore Ave S, Waterloo, On, Canada',
                'city' => 'Ontario',
                'category_id' => 2,
                'open_status' => false,
                'password' => bcrypt('12345678'),
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Bake Shop',
                'phone' => '333 3333 3333',
                'email' => 'bake@shop.com',
                'logo' => 'shop_rite.jpg',
                'banner' => '',
                'address' => '136 Moore Ave S, Waterloo, On, Canada',
                'city' => 'Ontario',
                'category_id' => 3,
                'open_status' => false,
                'password' => bcrypt('12345678'),
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
