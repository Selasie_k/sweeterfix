<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouriersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('couriers')->insert([
            'name' => 'Paul Kitsi',
            'email' => 'courier@sweeterfix.com',
            'phone' => '0542873229',
            'password' => bcrypt('12345678'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
