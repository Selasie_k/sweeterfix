<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            [
                'shop_id' => 1,
                'name' => 'Cakes',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 1,
                'name' => 'Cupcakes',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 1,
                'name' => 'Pastries',
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'shop_id' => 2,
                'name' => 'Cookies',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 2,
                'name' => 'Wafers',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 2,
                'name' => 'Assorted Tray',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 3,
                'name' => 'Savarian',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 3,
                'name' => 'Amandina',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 3,
                'name' => 'Isler',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 4,
                'name' => 'Bukuresti',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 4,
                'name' => 'Eclairs',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'shop_id' => 4,
                'name' => 'Cream Horns',
                'created_at' => now(),
                'updated_at' => now()
            ],
            
        ]);
    }
}
