<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_options')->insert([
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => '5 inch',
                'price' => 11.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => '6 inch',
                'price' => 15.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => '7 inch',
                'price' => 20.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => '8 inch',
                'price' => 25.5,
                'created_at' => now(),
                'updated_at' => now()
            ],



            // [
            //     'product_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '5 inch',
            //     'price' => 11.5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'product_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '6 inch',
            //     'price' => 15.5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'product_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '7 inch',
            //     'price' => 20.5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'product_id' => 3,
            //     'shop_id' => 1,
            //     'name' => '8 inch',
            //     'price' => 25.5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],

            [
                'product_id' => 2,
                'shop_id' => 1,
                'name' => 'Half dozen',
                'price' => 20.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 2,
                'shop_id' => 1,
                'name' => 'Dozen',
                'price' => 25.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
// 
            [
                'product_id' => 4,
                'shop_id' => 1,
                'name' => 'Half dozen',
                'price' => 20.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 4,
                'shop_id' => 1,
                'name' => 'Dozen',
                'price' => 25.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
