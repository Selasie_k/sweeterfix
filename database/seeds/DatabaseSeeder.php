<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            AdminsTableSeeder::class,
            CouriersTableSeeder::class,
            ShopCategoriesTableSeeder::class,
            ShopsTableSeeder::class,
            ProductCategoriesTableSeeder::class,
            ProductsTableSeeder::class,
            ProductAdditionsTableSeeder::class,
            ProductOptionsTableSeeder::class,
        ]);
    }
}
