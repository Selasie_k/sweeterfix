<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shop_categories')->insert([
            [
                'name' => 'Pastries',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Treats',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Healthy Snacks',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
