<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductAdditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_additions')->insert([
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => 'Extra Chocolate',
                'price' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => 'Extra Icing',
                'price' => 2.5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 1,
                'shop_id' => 1,
                'name' => 'Edible glitters',
                'price' => 0.5,
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'product_id' => 2,
                'shop_id' => 1,
                'name' => 'Thick Icing',
                'price' => 5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 2,
                'shop_id' => 1,
                'name' => 'Extra creamer',
                'price' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'product_id' => 4,
                'shop_id' => 1,
                'name' => 'Edible glitters',
                'price' => 0.5,
                'created_at' => now(),
                'updated_at' => now()
            ],

            [
                'product_id' => 4,
                'shop_id' => 1,
                'name' => 'Thick Icing',
                'price' => 5,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'product_id' => 4,
                'shop_id' => 1,
                'name' => 'Extra creamer',
                'price' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],

            // [
            //     'product_id' => 3,
            //     'shop_id' => 1,
            //     'name' => 'Thick Icing',
            //     'price' => 5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'product_id' => 3,
            //     'shop_id' => 1,
            //     'name' => 'Extra creamer',
            //     'price' => 4,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],

            // [
            //     'product_id' => 4,
            //     'shop_id' => 1,
            //     'name' => 'Extra Chocolate',
            //     'price' => 2,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'product_id' => 4,
            //     'shop_id' => 1,
            //     'name' => 'Extra Icing',
            //     'price' => 2.5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
            // [
            //     'product_id' => 4,
            //     'shop_id' => 1,
            //     'name' => 'Edible glitters',
            //     'price' => 0.5,
            //     'created_at' => now(),
            //     'updated_at' => now()
            // ],
        ]);
    }
}
