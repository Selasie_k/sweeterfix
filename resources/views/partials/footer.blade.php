<footer class="page-footer">
    <div class="container">
        <div class="row py-3 mx-5 text-center">
            <div class="col-sm-4 col-xs-6">
                <h5>About Us</h5>
                <h5>FAQs</h5>
                <h5>Contacts</h5> 
            </div>
            <div class="col-sm-4 col-xs-6">
                <h5>Privacy Policy</h5>
                <h5>Terms and Conditions</h5> 
                <h5>Leave us a Review</h5>
            </div>
            <div class="col-sm-4 col-xs-6">
                <h5>Connect with us</h5>
                <ul class="social-icons">
                    <li><a href="https://www.facebook.com/sweeterfix/"  target="_blank"><img src='/images/logos/facebook.svg' /></a></li>
                    <li><a href="https://www.instagram.com/sweeterfix/"  target="_blank"><img src='/images/logos/instagram.svg' /></a></li>
                    <li><a href="https://www.linkedin.com/company/sweeterfix/about/"  target="_blank"><img src='/images/logos/linkedin.svg' /></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3"><span class="text-muted">© 2019 Copyright:</span>
        <a href="#"> Sweeterfix Inc</a>
    </div>
</footer>