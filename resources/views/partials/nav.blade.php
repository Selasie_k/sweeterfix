{{-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel"> --}}
@if(isset($contrast_nav))
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
@else
    <nav class="navbar navbar-expand-md navbar-dark bg-transparent">
@endIf
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'SweeterFix') }}
        </a>

        <!-- Shopping Cart --> 
        {{-- <span class="ml-auto"><i><img src="/images/icons/shopping-cart.svg" alt="shopping-cart" id="cart"></i></span> --}}
        {{-- <span class="ml-auto"><i><img src="/images/icons/gift-box.svg" alt="shopping-bag" id="cart"></i></span> --}}

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse flex-grow-0" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links --> 
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        {{-- <a class="nav-link" data-toggle="modal" data-target="#loginModal">{{ __('Login') }}</a> --}}
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                            {{-- <a class="nav-link" data-toggle="modal" data-target="#userRegisterModal">{{ __('Sign Up') }}</a> --}}
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <span><i><img src="/images/icons/user.svg" alt="user avatar" id="avatar"></i></span>&nbsp;
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @auth('web')
                                <a class="dropdown-item" href="{{ route('user.profile', ['user' => Auth::id()]) }}">
                                    {{ __('My Account') }}
                                </a>
                            @endauth
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>