@component('mail::message')

Your order #**{{$order->number}}** has been placed with **{{$order->shop->name}}**

Below is your receipt and link to track your order

@component('mail::panel')
**{{$order->shop->name}}** 

_{{$order->shop->address}}_

@component('mail::table')
| Quantity    |   Item    |   Price   |
| ----------- | -------- | :-------- |
@foreach ($order->items as $item)
|   {{$item->quantity}}   |   {{$item->name}} ({{$item->option->name}})    |  **CA$ {{$item->subtotal}}**       |
@foreach ($item->additions as $addition)
| &nbsp;    |  &nbsp;<small> &bull; &nbsp; <i> {{$addition->name}}</i></small>| &nbsp;|
@endforeach
@endforeach
| &nbsp;    | **Subtotal:**    |    **CA$ {{$order->sub_total}}**   |
| &nbsp;    | **Service fee:**    |  **CA$ {{$order->service_fee}}**   |
| &nbsp;    | **Total:**    |   **CA$ {{$order->total}}**   |
@endcomponent


@endcomponent   

@component('mail::button', ['url' => route('order.track', ['order' => $order]), 'color' => 'sweet'])
Track Order
@endcomponent

You should expect delivery on .... between ... and ...

Regards, <br>
Sweeterfix Team
    
@endcomponent