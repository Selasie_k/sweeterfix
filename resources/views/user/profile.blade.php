@extends('layouts.app')

@section('content')
@include('partials.nav')
<div class="container my-4">
    {{-- <div class="category-strip">
        <ul>
            <li>My Account</li>
            <li>Orders</li>
            <li>Addresses</li>
            <li>Payment Options</li>
            <li>Support</li>
        </ul>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Hello {{auth()->user()->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    This is your dashboard
                </div>
            </div>
        </div>
    </div> --}}
    <nav>
        <div class="nav nav-tabs sfx-tabs justify-content-center" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-account-tab" data-toggle="tab" href="#nav-account" role="tab" aria-controls="nav-account" aria-selected="true">My Account</a>
            <a class="nav-item nav-link" id="#nav-orders-tab" data-toggle="tab" href="#nav-orders" role="tab" aria-controls="nav-orders" aria-selected="false">Orders</a>
            <a class="nav-item nav-link" id="nav-address-tab" data-toggle="tab" href="#nav-address" role="tab" aria-controls="nav-address" aria-selected="false">Addresses</a>
            <a class="nav-item nav-link" id="nav-payment-tab" data-toggle="tab" href="#nav-payment" role="tab" aria-controls="nav-payment" aria-selected="false">Payment Options</a>
            <a class="nav-item nav-link" id="nav-support-tab" data-toggle="tab" href="#nav-support" role="tab" aria-controls="nav-support" aria-selected="false">Support</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent" style="min-height: 80vh;">
        <div class="tab-pane fade show active" id="nav-account" role="tabpanel" aria-labelledby="nav-home-tab">
            <user-account :data="{{ $user }}"></user-account>
        </div>
        <div class="tab-pane fade" id="nav-orders" role="tabpanel" aria-labelledby="nav-profile-tab">
            <user-orders :prop_orders="{{ json_encode($user->orders) }}"></user-orders>
        </div>
        <div class="tab-pane fade" id="nav-address" role="tabpanel" aria-labelledby="nav-contact-tab">
            <user-addresses :user="{{ $user }}"></user-addresses>
        </div>
        <div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-contact-tab">
            <user-payment-options></user-payment-options>
        </div>
        <div class="tab-pane fade" id="nav-support" role="tabpanel" aria-labelledby="nav-contact-tab">
            <user-support></user-support>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function navigateToTab(){
        // console.log(window.location.hash)

        if(window.location.hash !== "") {
            document.getElementById(window.location.hash).click()
        }
    
    };
    window.onload = navigateToTab
</script>
@endsection
