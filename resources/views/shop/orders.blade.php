@extends('layouts.shop')

@section('content')
    <shop-orders :prop_orders="{{ json_encode($orders) }}"></shop-orders>  
@endsection
