@extends('layouts.app')

@section('content') 
    <section class="section-one">
        @include('partials.nav')
        <div class="jumbotron text-center vertical-center">
            <div class="container">
                <h1 class="cover-heading strip-text">An Easy Fix for Baked Goods Delivery</h1>
                <h3 class="lead">From The Bakery to Your Doorstep</h3>
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-md-10">
                        <p class="lead">
                            <search-location :ishomepage="true"></search-location>
                            {{-- <search-location ishomepage="{{ $isHomePage }}"></search-location> --}}
                            {{-- <example-component></example-component> --}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- <div class="text-center p-3">
        <p class="lead strip-text">HOW IT WORKS</p>
    </div> --}}
    <section class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="card mb-4 border-0 text-center">
                        <div class="card-body">
                            <img class="card-img-top py-2 homepage-icon" src="/images/location.svg" alt="Card image cap" >
                            <h5 class="card-title">Enter Your Address</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="card mb-4 border-0 text-center">
                        <div class="card-body">
                            <img class="card-img-top py-2 homepage-icon" src="/images/shop.svg" alt="Card image cap" >
                            <h5 class="card-title">Order From Bakeries</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="card mb-4 border-0 text-center">
                        <div class="card-body">
                            <img class="card-img-top py-2 homepage-icon" src="/images/credit-cards-payment.svg" alt="Card image cap" >
                            <h5 class="card-title">Pay Conveniently</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="card mb-4 border-0 text-center">
                        <div class="card-body">
                            <img class="card-img-top py-2 homepage-icon" src="/images/cake.svg" alt="Card image cap" >
                            <h5 class="card-title">It's An Easy Fix</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-three">
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12 text-center p-3">
                    <img src="/images/devices.png" alt="devices" id="devices">
                </div>
                <div class="col-sm-6 col-xs-12 text-left p-3 my-auto">
                    <h1 class="strip-text">DOWNLOAD OUR APP</h1>
                                    
                    <p class="lead">Our app gives you access to your favourite <br> restourants. Get your food delivered right to you</p>
                    <p class="lead">
                        <a href="#"><img src="/images/logos/appstore.png" alt="play logo" style="height: 2rem;"></a>
                        <a href="#"><img src="/images/logos/playstore.png" alt="play logo" style="height: 2rem;"></a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    {{-- <div class="text-center strip-text py-3">
        <p class="lead">Partner With Us</p>
    </div> --}}
    <section class="section-four pb-5">
        <div class="jumbotron text-left vertical-center">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h1 class="cover-heading strip-text">Partner With Us</h1>
                        <p class="lead">Join the SweeterFix team to deliver baked goods and smiles to customers. Partner with us. Let us help you deliver the baked goods while you focus on running your Bakery as efficiently as possible.</p>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-6 pr-3">
                                    <a href="#" class="btn btn-md btn-secondary">Bakery Sign Up</a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" class="btn btn-md btn-sweet">Courier Sign Up</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div> 
        </div>
    </section>
    {{-- <user-register></user-register>
    <bakery-register></bakery-register> --}}
@endsection

@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCL9Yzhhj4be97CO5Mnu03VN3GwTHpbE1w&libraries=places"></script>
@endsection