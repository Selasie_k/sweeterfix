@extends('layouts.app')

@section('content')
<div class="container-fluid min-vh-100 d-flex py-5" style="background-color: #e6e6e6;">

    <div class="row flex-column m-auto w-100 justify-content-center">

    <a class="pb-5 text-center" style="color: #ff6404; text-decoration: none;" href="{{url('/')}}"><h1>SweeterFix Admin</h1></a>
        <div class="col-sm-8 col-lg-3 mx-auto">
            <div class="card shadow-lg">
                <div class="card-header text-center">{{ __('Welcome Back!') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.login.submit') }}">
                        @csrf

                        <div class="form-group row">
                            {{-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> --}}

                            <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
 
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group row">
                            {{-- <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label> --}}

                            <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group text-center">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <button type="submit" class="btn btn-sweet btn-block">
                                {{ __('Login') }}
                            </button>
                        </div>
                        <div class="form-group row text-center">
                            @if (Route::has('password.request'))
                                <a class="btn btn-link mx-auto" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                        <hr class="my-0">
                        <div class="text-center">
                            <a class="btn btn-link" href="{{ route('register') }}">
                                {{ __('New to SweeterFix?   Register here') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
