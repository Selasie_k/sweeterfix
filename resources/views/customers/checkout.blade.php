@extends('layouts.app')

@section('content')
    @include('partials.nav')

    <div class="container min-vh-100">
        
        <div class="row py-3">

            <div class="col-sm-12 col-md-12 col-lg-8 mt-2 checkout-cards">

                <h1>Checkout</h1>

                <sweeterfix-checkout 
                    :time-window="{{json_encode($timeWindow)}}"
                    {{-- address="{{session('address')}}" --}}
                    :user="{{ $user }}"
                ></sweeterfix-checkout>

                {{-- <delivery-address></delivery-address>

                <checkout-payment></checkout-payment> --}}

            </div>
            
            <div class="col-sm-12 col-md-12 col-lg-4">
                <sweeterfix-cart 
                title="Continue shopping..." 
                checkout-page="true" 
                :deliveryfees="{{ json_encode($deliveryFees) }}"
                ></sweeterfix-cart>
            </div>

        </div>

    </div>
@endsection


@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCL9Yzhhj4be97CO5Mnu03VN3GwTHpbE1w&libraries=places"></script>
@endsection
