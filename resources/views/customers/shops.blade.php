@extends('layouts.app')

@section('content')
    <section class="bakeries-section-one">
        @include('partials.nav')
        <div class="jumbotron text-center">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-8 col-lg-6">
                    <p class="lead">
                        <search-location></search-location>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class=" col-xs-12 col-sm-12 col-md-10">
                    @foreach ($shops as $shop)
                        <bakery-component :shop="{{ json_encode($shop)}}"></bakery-component>
                    @endforeach
                </div>
            </div>
        </div>

    </section>
@endsection


@section('script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCL9Yzhhj4be97CO5Mnu03VN3GwTHpbE1w&libraries=places"></script>
@endsection