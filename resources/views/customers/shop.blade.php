@extends('layouts.app')

@section('content')

    {{-- <food-item-modal></food-item-modal> --}}

    <section class="bakeries-section-one">
        @include('partials.nav')
        <div class="jumbotron text-center">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-md-8 col-lg-6">
                    <div><img src="/images/logos/shop/{{$shop->logo}}" class="rounded mx-auto d-block banner-logo" alt="{{$shop->logo}}"></div>
                    <div class="bakery-banner-text">
                        <h1 class="cover-heading strip-text">{{ $shop->name }} </h1>
                        <h2>{{ $shop->category->name }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        
        <div class="category-strip sticky-top">
            <div class="container pb-2 pt-3 text-center d-flex">
                {{-- <span class="ml-auto" title="Your shopping cart" style="position: relative;">
                    <a href="#cart">
                        <span class="badge badge-dark cart-badge">0</span>
                        <img src="/images/icons/gift-box.svg" alt="shopping-bag" id="cart-icon">
                    </a>
                </span> --}}
                <cart-icon></cart-icon>

                <ul class="scrollable">
                    @foreach ($shop->product_categories as $category)
                        <li><a href="#{{str_replace(' ','',$category->name)}}">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
   
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8">
                    <div class="row">

                        @foreach ($shop->product_categories as $category)
                            <div class="container pt-4 pb-2 category-container" id="{{str_replace(' ','',$category->name)}}">
                                <h1 class="lead font-weight-bold">{{$category->name}}</h1>
                                <hr>
                            </div>

                            @foreach ($shop->products->where('category_id', $category->id) as $product)
                                <div class="col-sm-6 col-md-6 px-2">
                                    <shop-product :item="{{ json_encode($product) }}"></shop-product>
                                </div>
                            @endforeach
                        @endforeach
                       
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    {{-- <div class="cart sticky-top"  id="cart">
                        <div class="card">
                            <div class="card-header text-center">
                                <button class="btn btn-lg btn-sweet btn-block">Checkout</button>
                            </div>
                            <div class="card-body pb-0">
                                <div class="cart-item">
                                    <div class="row">
                                        <div class="col-3">
                                            <select class="custom-select">
                                                <option value="0">0</option>
                                                <option value="1" selected>1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="10">10</option>
                                            </select>
                                            <button class="btn btn-sm btn-block btn-dark mt-2">X</button>
                                        </div>
                                        <div class="col-6 pl-0">Butter Tarts (12 pcs)</div>
                                        <div class="col-3 px-0">CA$ 8.00</div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="cart-item">
                                    <div class="row">
                                        <div class="col-3">
                                            <input type="number" min="1" style="max-width: 100%;" value="1">
                                            <p class="lead mt-3">Remove</p>
                                        </div>
                                        <div class="col-6 pl-0">Assorted Monster Cookies</div>
                                        <div class="col-3 px-0">CA$ 89.00</div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <span class="lead">Subtotal</span>
                                <span>CA$ 00.00</span>
                            </div>
                        </div>
                    </div> --}}
                    <sweeterfix-cart title="Checkout"></sweeterfix-cart>
                </div>
            </div>
        </div>
    </section>
@endsection
