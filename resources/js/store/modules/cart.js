const state = {
    items: [],
    deliveryFees: [],
    distance: 8,
    deliveryCharges: [],
};

const getters = {
    items: state => state.items,

    shop_id: state => state.items.length ? state.items[0].shop_id : null, // TEMPORARY carful with this operation 

    cartLength: state => {
        return state.items.reduce((acc, item) => {
            return acc + parseInt(item.quantity, 10);
        }, 0);
    },

    // return subtotal of all items in the cart
    subTotal: state => {
        let subTotal = state.items.reduce((acc, item) => {
            return acc + item.subTotal * item.quantity;
        }, 0).toFixed(2);
        return parseFloat(subTotal);
    },

    // return servicefee = cart subtotal + charge associated with distance from shop
    serviceFee: state => {
        if (state.deliveryFees && state.deliveryFees.length) {
            let distanceToChargeFor = state.deliveryFees.filter(item => state.distance > item.min && state.distance <= item.max);
            return distanceToChargeFor.length ? parseFloat(distanceToChargeFor[0].fee) : 0;
        }
    },

    // return total of all items in cart plus service fee
    totalFee: (state, getters) => {
        let total = getters.subTotal + getters.serviceFee;
        return total;
    }
};

const actions = {

    fetchCart({ commit }) {
        const cart = wsCache.get('CART') ? wsCache.get('CART') : [];
        commit('setCart', cart);
    },

    pustToCart({ commit }, item) {
        const cart = wsCache.get('CART') ? wsCache.get('CART') : []; // get existing items in the cart
        cart.unshift(item) // push new item on the array 
        wsCache.set('CART', cart); // set the new array back to local storage
        console.log(wsCache.get('CART'));
        commit('updateCart', cart); //update vuex store state with updated cart
    },

    removeItem({ commit }, index) {
        commit('deleteItem', index);
    },

    // checkout page
    checkout({ commit }) {
        commit('saveCartToSession')
        window.location = '/checkout';
    },

    setDeliveryFees({ commit }, data) {
        commit('setDeliveryFees', data);
    },

    orderPlaced({ commit }){
        commit('PostShoppingCleanup');
    }
};

const mutations = {

    setCart: (state, cart) => (state.items = cart),

    updateCart: (state, cart) => {
        state.items = cart;
        // wsCache.set('CART', state.items); 
    },

    deleteItem: (state, index) => {
        state.items.splice(index, 1);
        wsCache.replace('CART', state.items);
    },

    saveCartToSession: (state) => {
        wsCache.replace('CART', state.items);
    },

    setDeliveryFees: (state, data) => (state.deliveryFees = data),

    PostShoppingCleanup: state => {
        state.items = [];
        wsCache.delete('CART');
        wsCache.delete('location');
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
