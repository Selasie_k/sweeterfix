/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// import VueMaterial from 'vue-material';
// Vue.use(VueMaterial.mdButton);

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component(
    "bakery-component",
    require("./components/customers/Bakery.vue").default
);
Vue.component(
    "search-location",
    require("./components/customers/Address.vue").default
);
Vue.component(
    "sweeterfix-cart",
    require("./components/customers/Cart.vue").default
);
Vue.component(
    "shop-product",
    require("./components/customers/ShopProduct.vue").default
);
Vue.component(
    "food-item-modal",
    require("./components/FoodItemModalComponent.vue").default
);
Vue.component(
    "cart-icon",
    require("./components/customers/CartIcon.vue").default
);
Vue.component(
    "cart-item",
    require("./components/customers/CartItem.vue").default
);

Vue.component(
    "sweeterfix-checkout",
    require("./components/customers/checkout/Checkout.vue").default
);
Vue.component( "stripe-payment", require("./components/customers/checkout/Stripe.vue").default);

Vue.component( "user-account", require("./components/users/Account.vue").default);
Vue.component( "user-orders", require("./components/users/Orders.vue").default);
Vue.component( "user-addresses", require("./components/users/Addresses.vue").default);
Vue.component( "user-payment-options", require("./components/users/PaymentOptions.vue").default);
Vue.component( "user-support", require("./components/users/Support.vue").default);

Vue.component( "shop-orders", require("./components/shops/Orders.vue").default);

import Datepicker from "vuejs-datepicker";
Vue.use(Datepicker);

import moment from "moment";
window.moment = moment;

import swal from 'sweetalert2';
window.Swal = swal;

window.Toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

import WebStorageCache from "web-storage-cache";
window.wsCache = new WebStorageCache({
    // [option] 'localStorage', 'sessionStorage', window.localStorage, window.sessionStorage or
    // other storage instance implement [Storage API].
    // default 'localStorage'.
    storage: "sessionStorage",
    // [option] //An expiration time, in seconds. default never .
    exp: 86400
});

// import vuex store
import store from "./store";

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import axios from "axios";
// Vue.use(axios);

window.eventBus = new Vue();

const app = new Vue({
    store,
    el: "#app"
});

/**
 * smooth scrolling to anchor div
 */

$('a[href^="#"]').on("click", function(event) {
    var target = $(this.getAttribute("href")),
        fixedElementHeight = 70;

    if (target.length) {
        event.preventDefault();
        $("html, body").animate(
            {
                scrollTop: target.offset().top - fixedElementHeight
            },
            1000
        );
    }
});
